import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExecutorasRoutingModule } from './executoras-routing.module';
import {MenuModule} from '../../../../core/components/menu/menu.module';
import {ExecutorasComponent} from './executoras.component';
import {MessageModule} from '../../../../core/components/message/message.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {MessageService} from '../../../../core/services/message.service';

@NgModule({
  declarations: [ExecutorasComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ExecutorasRoutingModule,
    MenuModule,
    AutocompleteLibModule,
    MessageModule
  ],
  providers: [
    MessageService
  ]
})
export class ExecutorasModule { }
