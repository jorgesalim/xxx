import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutorasComponent } from './executoras.component';

describe('ExecutorasComponent', () => {
  let component: ExecutorasComponent;
  let fixture: ComponentFixture<ExecutorasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutorasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutorasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
