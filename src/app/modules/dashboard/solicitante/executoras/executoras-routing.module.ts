import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RoutersGuard} from '../../../../core/guard/routers.guard';
import {ExecutorasComponent} from './executoras.component';

const routes: Routes = [{
  path: '',
  component: ExecutorasComponent,
  canActivate: [RoutersGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExecutorasRoutingModule { }
