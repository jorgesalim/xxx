import { Component, OnInit } from '@angular/core';
import {ExecutoraService} from '../../../../core/services/executora.service';
import {Executora} from '../../../../core/model/executora';
import {Mensagem} from '../../../../core/model/mensagem';
import {ClassActive} from '../../../../core/utils/classActive';
import {MessageService} from '../../../../core/services/message.service';

@Component({
  selector: 'app-executoras',
  templateUrl: './executoras.component.html',
  styleUrls: ['./executoras.component.css']
})
export class ExecutorasComponent implements OnInit {

  start = 0;
  limit = 3;
  public executorasSearch: Executora[];
  public executora: Executora;
  public mensagem: Mensagem;
  public classActive: ClassActive;
  public classIncluir: string;
  public executoras: Executora[];
  keyword = 'nome';

  constructor(public executoraService: ExecutoraService,
              public messageService: MessageService) { }

  ngOnInit() {
    this.executora = new Executora();
    this.classActive = new ClassActive();
    this.mensagem = new Mensagem('', '', '');
    this.list(this.start, this.limit);
    this.executorasSearch = [];
  }

  onSubmit() {
    this.executoraService.insert(this.executora)
        .subscribe((response) => {
              this.reset(
                  'done',
                  'Executora cadastrada com sucesso',
                  'active');
            }, (error) => {
              console.log(error);
              if (!error.ok) {
                if (error.status === 400) {
                  this.mensagem = new Mensagem(
                      'error',
                      'Problemas na gravação da executora',
                      'active');
                }
              }
            }
        );
  }

  onUpdate(executora: Executora) {
    this.executoraService.update(executora)
        .subscribe((response) => {
              this.reset(
                  'done',
                  'Executora atualizada com sucesso',
                  'active');
            }, (error) => {
              console.log(error);
              if (!error.ok) {
                if (error.status === 400) {
                  this.setMessage(
                      'error',
                      'Problemas na atualização da executora',
                      'active');
                }
              }
            }
        );
  }

  onDelete(executora: Executora) {
    this.executoraService.delete(executora).subscribe((response) => {
        this.setMessage(
            'alert',
            'Executora excluída com sucesso',
            'active');
    }, (error) => {
        console.log(error);
        if (!error.ok) {
            if (error.status === 400) {
                this.setMessage(
                    'error',
                    'Problemas na exclusão da executora',
                    'active');
            }
        }
    });
  }

  list(start, limit) {
    this.executoraService.listLimit(start, limit).subscribe(executoras => this.executoras = (executoras));
  }

  listMore(start, limit) {
    this.executoraService.listLimit(start, limit).subscribe(executoras => this.executoras = this.executoras.concat(executoras));
  }

  selectEvent(item) {
    // do something with selected item
    this.executoras = [item];
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    console.log(search);
    this.executoraService.listByNameLike(search).subscribe(executoras => {
      this.executoraService.listByBairroLike(search).subscribe(executorasBairro => {
        if (executoras.length > 0) {
          const novaExecutorasSearch = executorasBairro.filter(x => {
            return executoras.find( e => e.id === x.id) ? true : false;
          });
          this.executorasSearch = novaExecutorasSearch.length === 0 ? (executoras) : novaExecutorasSearch;
        } else {
          this.executorasSearch = executorasBairro;
        }
      });
    });
  }

  onFocused(e) {
    // do something
  }

  private reset(type: string, description: string, active: string) {
    this.setMessage(type, description, active);
    this.executora = new Executora();
    this.start = 0;
    this.limit = 3;
    this.list(this.start, this.limit);
    this.classIncluir = '';
  }

  private setMessage(type: string, description: string, active: string) {
    this.messageService.add(new Mensagem(type, description, active));
  }

}
