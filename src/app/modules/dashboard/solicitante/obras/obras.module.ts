import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ObrasRoutingModule } from './obras-routing.module';
import {ObrasComponent} from './obras.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MessageModule} from '../../../../core/components/message/message.module';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';
import {MenuModule} from '../../../../core/components/menu/menu.module';

@NgModule({
  declarations: [ObrasComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    ObrasRoutingModule,
    MenuModule,
    AutocompleteLibModule,
    MessageModule
  ]
})
export class ObrasModule { }
