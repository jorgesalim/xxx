import {Component, OnInit} from '@angular/core';
import {ObraService} from '../../../../core/services/obra.service';
import {MessageService} from '../../../../core/services/message.service';
import {ClassActive} from '../../../../core/utils/classActive';
import {Mensagem} from '../../../../core/model/mensagem';
import {Obra} from '../../../../core/model/obra';
import {Trecho} from '../../../../core/model/trecho';
import {TrechoService} from '../../../../core/services/trecho.service';
import {UserService} from '../../../../core/services/user.service';
import {Observable} from 'rxjs';
import {User} from '../../../../core/model/user';
import {ExecutoraService} from '../../../../core/services/executora.service';
import {Executora} from '../../../../core/model/executora';

@Component({
  selector: 'app-obras',
  templateUrl: './obras.component.html',
  styleUrls: ['./obras.component.css']
})
export class ObrasComponent implements OnInit {

  start = 0;
  limit = 3;
  public obrasSearch: Obra[];
  public obra: Obra;
  public mensagem: Mensagem;
  public classActive: ClassActive;
  public classIncluir: string;
  public obras: Obra[];
  public engenheiros: User[];
  public geologos: User[];
  public executoras: Executora[];
  public trecho: Trecho;
  keyword = 'nomeobra';

  constructor(public obraService: ObraService,
              public executoraService: ExecutoraService,
              public userService: UserService,
              public messageService: MessageService) {
  }

  ngOnInit() {
    this.obra = new Obra();
    this.obra.executora = new Executora();
    this.obra.engenheiro = new User();
    this.obra.geologo = new User();
    this.classActive = new ClassActive();
    this.mensagem = new Mensagem('', '', '');
    this.list(this.start, this.limit);
    this.obrasSearch = [];
    this.setEngenheiros();
    this.setGeologos();
    this.executoraService.list().subscribe(executoras => this.executoras = executoras);
  }

  onSubmit() {
    this.obra.status = 'Aguardando';
    this.obraService.insert(this.obra)
      .subscribe((response) => {
          this.reset(
            'done',
            'Obra cadastrada com sucesso',
            'active');
        }, (error) => {
          console.log(error);
          if (!error.ok) {
            if (error.status === 400) {
              this.mensagem = new Mensagem(
                'error',
                'Problemas na gravação da obra',
                'active');
            }
          }
        }
      );
  }

  onUpdate(obra: Obra) {
    this.obraService.update(obra)
      .subscribe((response) => {
          this.reset(
            'done',
            'Obra atualizada com sucesso',
            'active');
        }, (error) => {
          console.log(error);
          if (!error.ok) {
            if (error.status === 400) {
              this.setMessage(
                'error',
                'Problemas na atualização da executora',
                'active');
            }
          }
        }
      );
  }

  onDelete(obra: Obra) {
    this.obraService.delete(obra).subscribe((response) => {
      this.setMessage(
        'alert',
        'Obra excluída com sucesso',
        'active');
    }, (error) => {
      console.log(error);
      if (!error.ok) {
        if (error.status === 400) {
          this.setMessage(
            'error',
            'Problemas na exclusão da obra',
            'active');
        }
      }
    });
  }

  list(start, limit) {
    this.obraService.listLimit(start, limit).subscribe(obras => {
      this.obras = obras.filter(o => {
        o.datainicio = o.datainicio.slice(0, 10);
        o.datafim = o.datafim.slice(0, 10);
        if (o.engenheiro === null) {
          o.engenheiro = new User();
        }
        if (o.geologo === null) {
          o.geologo = new User();
        }
        if (o.executora === null) {
          o.executora = new Executora();
        }
        return true;
      });
    });
  }

  listMore(start, limit) {
    this.obraService.listLimit(start, limit).subscribe(obras => {
      this.obras = this.obras.concat(obras.filter(o => {
        o.datainicio = o.datainicio.slice(0, 10);
        o.datafim = o.datafim.slice(0, 10);
        if (o.engenheiro === null) {
          o.engenheiro = new User();
        }
        if (o.geologo === null) {
          o.geologo = new User();
        }
        if (o.executora === null) {
          o.executora = new Executora();
        }
        return true;
      }));
    });
  }

  listUsersByRoleName(roleName: string): Observable<User[]> {
    return this.userService.listByRoleName(roleName);
  }

  setEngenheiros() {
    this.listUsersByRoleName('Engenheiro').subscribe(users => this.engenheiros = users);
  }

  setGeologos() {
    this.listUsersByRoleName('Geologo').subscribe(users => this.geologos = users);
  }

  selectEvent(item) {
    // do something with selected item
    this.obras = [item];
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    console.log(search);
    this.obraService.listByNameLike(search).subscribe(obras => {
      this.obrasSearch = obras.filter(o => {
      o.datainicio = o.datainicio.slice(0, 10);
      o.datafim = o.datafim.slice(0, 10);
      if (o.engenheiro === null) {
        o.engenheiro = new User();
      }
      if (o.geologo === null) {
        o.geologo = new User();
      }
      if (o.executora === null) {
        o.executora = new Executora();
      }
      return true;
    });
    });
  }

  selectExecutora(item) {
    // do something with selected item
    this.obra.executora = item;
  }

  onChangeSearchExecutora(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    console.log(search);
    this.executoraService.listByNameLike(search).subscribe(executoras => this.executoras = executoras);
  }

  listExecutoras(start, limit) {
    this.executoraService.listLimit(start, limit).subscribe(executoras => this.executoras = executoras);
  }

  onFocused(e) {
    // do something
  }

  private reset(type: string, description: string, active: string) {
    this.setMessage(type, description, active);
    this.obra = new Obra();
    this.obra.engenheiro = new User();
    this.obra.geologo = new User();
    this.obra.executora = new Executora();
    this.start = 0;
    this.limit = 3;
    this.list(this.start, this.limit);
    this.classIncluir = '';
  }

  private setMessage(type: string, description: string, active: string) {
    this.messageService.add(new Mensagem(type, description, active));
  }


}
