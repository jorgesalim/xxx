import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrechosRoutingModule } from './trechos-routing.module';
import { TrechosComponent } from './trechos.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MessageModule} from '../../../../core/components/message/message.module';
import {AutocompleteLibModule} from 'angular-ng-autocomplete';

@NgModule({
  declarations: [TrechosComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MessageModule,
    AutocompleteLibModule,
    TrechosRoutingModule
  ]
})
export class TrechosModule { }
