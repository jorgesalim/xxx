import { Component, OnInit } from '@angular/core';
import {ObraService} from '../../../../core/services/obra.service';
import {TrechoService} from '../../../../core/services/trecho.service';
import {Obra} from '../../../../core/model/obra';
import {ClassActive} from '../../../../core/utils/classActive';
import {User} from '../../../../core/model/user';
import {Executora} from '../../../../core/model/executora';
import {Trecho} from '../../../../core/model/trecho';
import {Mensagem} from '../../../../core/model/mensagem';
import {MessageService} from '../../../../core/services/message.service';

@Component({
  selector: 'app-trechos',
  templateUrl: './trechos.component.html',
  styleUrls: ['./trechos.component.css']
})
export class TrechosComponent implements OnInit {

  start = 0;
  limit = 3;

  obras: Obra[];
  mensagem: Mensagem;
  public trechoInsert: Trecho[];
  public classActive: ClassActive;
  obrasSearch: Obra[];
  keyword = 'nomeobra';

  constructor(public obraService: ObraService,
              public trechoService: TrechoService,
              public messageService: MessageService) { }

  ngOnInit() {
    this.trechoInsert = [];
    this.classActive = new ClassActive();
    this.list(this.start, this.limit);
  }

  onSubmit(trecho, idObra) {
    trecho.obra = idObra;
    trecho.status = 'Aguardando';
    this.trechoService.insert(trecho).subscribe((response) => {
        this.reset(
          'done',
          'Trecho cadastrado com sucesso',
          'active');
      }, (error) => {
        console.log(error);
        if (!error.ok) {
          if (error.status === 400) {
            this.mensagem = new Mensagem(
              'error',
              'Problemas na gravação do trecho',
              'active');
          }
        }
      }
    );
  }

  onUpdate(trecho, idObra) {
    trecho.obra = idObra;
    this.trechoService.update(trecho).subscribe((response) => {
        this.reset(
          'done',
          'Trecho atualizado com sucesso',
          'active');
      }, (error) => {
        console.log(error);
        if (!error.ok) {
          if (error.status === 400) {
            this.mensagem = new Mensagem(
              'error',
              'Problemas na atualização do trecho',
              'active');
          }
        }
      }
    );
  }

  list(start, limit) {
    this.obraService.listLimit(start, limit).subscribe(obras => {
      this.obras = obras.filter(o => {
        o.trechos.filter(t => {
          t.active = '';
          t.edit = '';
          t.disable = false;
          return true;
        });
        return true;
      });
      for (let i = 0; i < obras.length; i ++) {
        this.trechoInsert[i] = (new Trecho());
      }
    });
  }

  listMore(start, limit) {
    this.obraService.listLimit(start, limit).subscribe(obras => {
      obras = obras.filter(o => {
        o.trechos.filter(t => {
          t.active = '';
          t.edit = '';
          t.disable = false;
          return true;
        });
        return true;
      });
      this.obras = this.obras.concat(obras);
      for (let i = 0; i < (this.obras.length); i ++) {
        this.trechoInsert[i] = (new Trecho());
      }
    });
  }

  collapse(trecho) {
    trecho.active = this.classActive.active(trecho.active); trecho.edit = ''; trecho.disable = true;
  }

  selectEvent(item) {
    // do something with selected item
    this.obras = [item];
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    console.log(search);
    this.obraService.listByNameLike(search).subscribe(obras => this.obrasSearch = obras);
  }

  onFocused(e) {
    // do something
  }

  private setMessage(type: string, description: string, active: string) {
    this.messageService.add(new Mensagem(type, description, active));
  }

  private reset(type: string, description: string, active: string) {
    this.setMessage(type, description, active);
    this.start = 0;
    this.limit = 3;
    this.list(this.start, this.limit);
  }
}
