import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrechosComponent } from './trechos.component';

describe('TrechosComponent', () => {
  let component: TrechosComponent;
  let fixture: ComponentFixture<TrechosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrechosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrechosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
