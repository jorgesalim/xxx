import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RoutersGuard} from '../../../../core/guard/routers.guard';
import {TrechosComponent} from './trechos.component';

const routes: Routes = [{
  path: '',
  component: TrechosComponent,
  canActivate: [RoutersGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrechosRoutingModule { }
