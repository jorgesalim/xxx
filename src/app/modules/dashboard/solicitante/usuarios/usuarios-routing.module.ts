import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RoutersGuard} from '../../../../core/guard/routers.guard';
import {UsuariosComponent} from './usuarios.component';

const routes: Routes = [{
  path: '',
  component: UsuariosComponent,
  canActivate: [RoutersGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosRoutingModule { }
