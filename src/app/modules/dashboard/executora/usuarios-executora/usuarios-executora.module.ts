import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsuariosExecutoraRoutingModule } from './usuarios-executora-routing.module';
import { UsuariosExecutoraComponent } from './usuarios-executora.component';

@NgModule({
  declarations: [UsuariosExecutoraComponent],
  imports: [
    CommonModule,
    UsuariosExecutoraRoutingModule
  ]
})
export class UsuariosExecutoraModule { }
