import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuariosExecutoraComponent } from './usuarios-executora.component';

describe('UsuariosExecutoraComponent', () => {
  let component: UsuariosExecutoraComponent;
  let fixture: ComponentFixture<UsuariosExecutoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsuariosExecutoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuariosExecutoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
