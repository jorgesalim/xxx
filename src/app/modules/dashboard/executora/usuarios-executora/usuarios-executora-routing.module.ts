import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RoutersGuard} from '../../../../core/guard/routers.guard';
import { UsuariosExecutoraComponent } from './usuarios-executora.component';

const routes: Routes = [{
  path: '',
  component: UsuariosExecutoraComponent,
  canActivate: [RoutersGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsuariosExecutoraRoutingModule { }
