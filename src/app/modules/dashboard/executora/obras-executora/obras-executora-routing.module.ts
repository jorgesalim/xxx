import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RoutersGuard} from '../../../../core/guard/routers.guard';
import { ObrasExecutoraComponent } from './obras-executora.component';

const routes: Routes = [{
  path: '',
  component: ObrasExecutoraComponent,
  canActivate: [RoutersGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObrasExecutoraRoutingModule { }
