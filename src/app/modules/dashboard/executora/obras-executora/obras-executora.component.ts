import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../../core/services/auth.service';

import { ObraService } from '../../../../core/services/obra.service';
import { Obra } from '../../../../core/model/obra';
import { ClassActive } from '../../../../core/utils/classActive';
import { Trecho } from '../../../../core/model/trecho';
import { MessageService } from '../../../../core/services/message.service';
import { Mensagem } from '../../../../core/model/mensagem';
import { TrechoService } from '../../../../core/services/trecho.service';
import { UserService } from '../../../../core/services/user.service';
import { Observable } from 'rxjs';
import { User } from '../../../../core/model/user';
import { Executora } from '../../../../core/model/executora';
import {Auth} from '../../../../core/model/auth';

@Component({
  selector: 'app-obras-executora',
  templateUrl: './obras-executora.component.html',
  styleUrls: ['./obras-executora.component.css']
})
export class ObrasExecutoraComponent implements OnInit {

  auth: Auth;
  obras: Obra[];
  obrasSearch: Obra[];
  engenheiros: User[];
  geologos: User[];
  coletores: User[];
  keyword = 'nomeobra';
  classActive: ClassActive;
  adminType = 'Administrator';
  executoraType = 'Executora';
  start = 0;
  limit = 3;

  constructor(
    private authService: AuthService,
    public userService: UserService,
    private trechoService: TrechoService,
    private obraService: ObraService,
    private messageService: MessageService
  ) {
    this.auth = JSON.parse(localStorage.getItem('auth'));
  }

  ngOnInit() {
    this.engenheiros = [];
    this.geologos = [];
    this.obras = [];
    this.obrasSearch = [];
    this.classActive = new ClassActive();
    this.list(this.start, this.limit); // Irá setar o id do usuário logado
    this.setEngenheiros();
    this.setGeologos();
    this.setColetores();
  }

  selectEvent(item) {
    // do something with selected item
    this.obras = [item];
  }

  onChangeSearch(search: string) {
    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
    console.log(search);
    this.obraService.listByNomeObraLike(search).subscribe(obras => {
        if (obras.length > 0) {
          this.obrasSearch = obras.length === 0 ? [] : obras;
        } else {
          this.obrasSearch = obras;
        }
    });
  }

  onFocused(e) {
    // do something
  }

  onUpdateTrecho(trecho: Trecho) {
    this.deleteAttributes(trecho);
    this.trechoService.update(trecho)
      .subscribe((response) => {
        this.setUserIntoObra(trecho.obra, trecho['coletor']);
        this.setMessage(
            'done',
            'Trecho atualizado com sucesso',
            'active');
        }, (error) => {
          console.log(error);
          if (!error.ok) {
            if (error.status === 400) {
              this.setMessage(
                'error',
                'Problemas na atualização do trechoInsert',
                'active');
            }
          }
        }
      );
  }

  setUserIntoObra(id, userId) {
    let obra = this.obras.filter(o => o.id === id)[0];
    obra.user = userId;
    this.obraService.update(obra).subscribe(() => {});
  }

  onUpdateObra(obra: Obra) {
    this.deleteAttributes(obra);
    this.obraService.update(obra)
      .subscribe((response) => {
        this.setMessage(
            'done',
            'Obra atualizada com sucesso',
            'active');
        }, (error) => {
          console.log(error);
          if (!error.ok) {
            if (error.status === 400) {
              this.setMessage(
                'error',
                'Problemas na atualização da obra',
                'active');
            }
          }
        }
      );
  }

  listMore(start, limit) {
    let type = this.authService.authentication.user.role.name;

    if (type === this.executoraType) {
      this.getLimitByExecutoraIdLike(start, limit);
    } else if (type === this.adminType) {
      this.getLimitLike(start, limit);
    }
  }

  getLimitByExecutoraIdLike(start, limit) {
    let id = this.authService.authentication.user.executoras;

    this.obraService.listLimitByExecutoraIdLike(id, start, limit).subscribe(obras => {
      this.obras = this.obras.concat(obras.filter(o => {
        o.datainicio = o.datainicio.slice(0, 10);
        o.datafim = o.datafim.slice(0, 10);
        if (o.engenheiro === null) {
          o.engenheiro = new User();
        }
        if (o.geologo === null) {
          o.geologo = new User();
        }
        if (o.executora === null) {
          o.executora = new Executora();
        }
        return true;
      }));
    });
  }

  getLimitLike(start, limit) {
    this.obraService.listLimit(start, limit).subscribe(obras => {
      this.obras = this.obras.concat(obras.filter(o => {
        o.datainicio = o.datainicio.slice(0, 10);
        o.datafim = o.datafim.slice(0, 10);
        if (o.engenheiro === null) {
          o.engenheiro = new User();
        }
        if (o.geologo === null) {
          o.geologo = new User();
        }
        if (o.executora === null) {
          o.executora = new Executora();
        }
        return true;
      }));
    });
  }

  collapseMap(type) {
    if (type.active) {
      type.active = '';
      type.edit = '';
      type.disable = true;
    } else {
      type.active = 'active';
    }
  }

  cancel(type) {
    type.edit = '';
    type.disable = true;
  }

  list(start, limit) {
    let type = this.authService.authentication.user.role.name;

    if (type === this.executoraType) {
      this.listLimitByExecutoraIdLike(start, limit);
    } else if (type === this.adminType) {
      this.listLimit(start, limit);
    }    
  }

  listLimitByExecutoraIdLike(start, limit) {
    let id = this.authService.authentication.user.executoras;

    this.obraService.listLimitByExecutoraIdLike(id, start, limit).subscribe(obras => {
      this.obras = this.obras.concat(obras.filter(o => {
        o.datainicio = o.datainicio.slice(0, 10);
        o.datafim = o.datafim.slice(0, 10);
        if (o.engenheiro === null) {
          o.engenheiro = new User();
        }
        if (o.geologo === null) {
          o.geologo = new User();
        }
        if (o.executora === null) {
          o.executora = new Executora();
        }
        return true;
      }));
    });
  }

  listLimit(start, limit) {    
    this.obraService.listLimit(start, limit).subscribe(obras => {
      this.obras = this.obras.concat(obras.filter(o => {
        o.datainicio = o.datainicio.slice(0, 10);
        o.datafim = o.datafim.slice(0, 10);
        if (o.engenheiro === null) {
          o.engenheiro = new User();
        }
        if (o.geologo === null) {
          o.geologo = new User();
        }
        if (o.executora === null) {
          o.executora = new Executora();
        }
        return true;
      }));
    });
  }

  listUsersByRoleName(roleName: string): Observable<User[]> {
    return this.userService.listByRoleName(roleName);
  }

  setEngenheiros() {
    this.listUsersByRoleName('Engenheiro').subscribe(users => this.engenheiros = users);
  }

  setGeologos() {
    this.listUsersByRoleName('Geologo').subscribe(users => this.geologos = users);
  }

  setColetores() {
    this.listUsersByRoleName('Coletores').subscribe(users => this.coletores = users);
  }

  private setMessage(type: string, description: string, active: string) {
    this.messageService.add(new Mensagem(type, description, active));
  }

  private deleteAttributes(obj: any) {
    delete obj['active'];
    delete obj['edit'];
    delete obj['disable'];
  }

}
