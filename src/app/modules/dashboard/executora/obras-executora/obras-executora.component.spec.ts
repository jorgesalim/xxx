import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObrasExecutoraComponent } from './obras-executora.component';

describe('ObrasExecutoraComponent', () => {
  let component: ObrasExecutoraComponent;
  let fixture: ComponentFixture<ObrasExecutoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObrasExecutoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObrasExecutoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
