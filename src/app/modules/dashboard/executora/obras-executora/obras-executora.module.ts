import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObrasExecutoraRoutingModule } from './obras-executora-routing.module';
import { ObrasExecutoraComponent } from './obras-executora.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessageModule } from '../../../../core/components/message/message.module';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

@NgModule({
  declarations: [ObrasExecutoraComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MessageModule,
    AutocompleteLibModule,
    ObrasExecutoraRoutingModule
  ]
})
export class ObrasExecutoraModule { }
