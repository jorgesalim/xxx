import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TimelineComponent} from './timeline.component';
import {RoutersGuard} from '../../core/guard/routers.guard';

const routes: Routes = [{
  path: '',
  component: TimelineComponent,
  canActivate: [RoutersGuard]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimelineRoutingModule { }
