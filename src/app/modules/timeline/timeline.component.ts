import { Component, OnInit } from '@angular/core';
import {ObraService} from '../../core/services/obra.service';
import {Obra} from '../../core/model/obra';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
  obras: Obra[];
  maps: Obra[];
  obra: Obra;
  start = 0;
  limit = 3;
  lat = -22.9116;
  lng = -43.1883;
  activeDaily = '';
  activeTrecho = '';
  activeMaps = '';

  constructor(public obraService: ObraService) { }

  ngOnInit() {
    this.list(this.start, this.limit);
    console.log('init');
  }

  list(start, limit) {
    this.obraService.listLimit(start, limit).subscribe(obras => this.encapsuletList(obras));
  }

  listMore(start, limit) {
    this.obraService.listLimit(start, limit).subscribe(obras => {
      this.obras = this.obras.concat(obras)
      this.maps = this.obras;
    });
  }

  add() {
    if (this.obra) {
      this.obraService.getById(this.obra.id).subscribe(obra => this.encapsuletObject(obra));
    }
  }

  status(value) {
    if (value === 'Aguardando') {
      return 'not-started';
    } else if (value === 'Construindo') {
      return 'in-progress';
    } else {
      return 'finalized';
    }
  }

  active(type) {
    if (type === 'trechoInsert') {
      this.activeTrecho = this.activeTrecho === 'active' ? '' : 'active';
    } else if (type === 'maps') {
      this.activeMaps = this.activeMaps === 'active' ? '' : 'active';
    } else {
      this.activeDaily = this.activeDaily === 'active' ? '' : 'active';
    }
  }

  collapseMaps(collapse) {
    const elementMap = this.newElementType(this.getMapTag());
    if (collapse) {
      elementMap.style.display = 'block';
    } else {
      elementMap.style.display = 'none';
    }
  }

  getMapTag() {
    return document.getElementsByTagName('agm-map')[0];
  }

  newElementType(el) {
    return el;
  }

  resetMaps() {
    this.maps = this.obras;
  }

  private encapsuletList(obras) {
    this.obras = obras;
    this.maps = obras;
  }

  private encapsuletObject(obra) {
    this.obras = [obra];
    this.maps = [obra];
  }

}
