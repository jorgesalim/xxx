import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimelineRoutingModule } from './timeline-routing.module';
import {TimelineComponent} from './timeline.component';
import {MenuModule} from '../../core/components/menu/menu.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgSelectModule} from '@ng-select/ng-select';

import {AgmCoreModule} from '@agm/core';
import {AgmJsMarkerClustererModule} from '@agm/js-marker-clusterer';

@NgModule({
  declarations: [
    TimelineComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC5TvqWT-CqOV12BR8ccOWr8x_ooU8WdUc'
    }),
    AgmJsMarkerClustererModule,
    CommonModule,
    TimelineRoutingModule,
    MenuModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule
  ]
})
export class TimelineModule { }
