import {Component, Input, OnInit} from '@angular/core';
import {Obra} from '../../core/model/obra';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {

  @Input() obra: Obra;

  constructor() { }

  ngOnInit() {
  }

}
