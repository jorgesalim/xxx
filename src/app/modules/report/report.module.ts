import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { ReportComponent } from './report.component';
import {WorkRoutingModule} from '../work/work-routing.module';
import {FormsModule} from '@angular/forms';
import {MessageModule} from '../../core/components/message/message.module';
import {MenuModule} from '../../core/components/menu/menu.module';

@NgModule({
  declarations: [ReportComponent],
  imports: [
    CommonModule,
    FormsModule,
    MessageModule,
    MenuModule,
    ReportRoutingModule
  ]
})
export class ReportModule { }
