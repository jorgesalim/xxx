import { Component, OnInit } from '@angular/core';
import {Auth} from '../../core/model/auth';
import {Router} from '@angular/router';
import {Login} from '../../core/model/login';
import {MessageService} from '../../core/services/message.service';
import {Mensagem} from '../../core/model/mensagem';
import {AuthService} from '../../core/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public type: string;
  public description: string;
  public active: string;
  private auth: Auth;
  public login: Login;
  public mensagem: Mensagem;
  constructor(private authService: AuthService,
              private router: Router,
              public messageService: MessageService) { }

  ngOnInit() {
    this.login = new Login();
    this.authService.authentication = JSON.parse(localStorage.getItem('auth'));
    if (this.authService.authentication) {
      this.router.navigate(['/time-line']);
    }

    this.bgActive();
    document.addEventListener('mousemove', event => {
      this.bgAnimate(event);
    });
  }

  onSubmit() {
    this.authService.login(this.login)
      .subscribe((response) => {
        this.auth = response;
        localStorage.setItem('auth', JSON.stringify(response));
        if (this.auth.user.role.name === 'Executora') {
          this.router.navigate(['/dashboard/executora/obras']);
        } else {
          this.router.navigate(['/time-line']);
        }
      }, (error) => {
        console.log(error);
        if (!error.ok) {
          if (error.status === 400) {
            this.messageService.add(new Mensagem('alert',
                'Usuário ou senha inválidos',
                'active'));
          }
        }
      }
      );
  }

  bgActive() {
    if (document.getElementById('dbg')) {
      const el = document.getElementById('dbg');

      const el2 = el.children;

      setInterval(() => {
        for (let i = 0; i < el2.length; i++) {
          let j = i + 1;
          if (el2[i].classList.contains('active')) {
            el2[i].classList.remove('active');

            if (j > i && j < el2.length) {
              el2[j].classList.add('active');
              break;
            } else {
              el2[el2.length - el2.length].classList.add('active');
              break;
            }
          }
        }
      }, 15000);
    }
  }

  bgAnimate(event) {

    const el = document.getElementById('dbg');
    if (!el) return;
    const el2 = el.children;

    Object.keys(el2).forEach(i => {
      if (el2[i].classList.contains('active')) {
        el2[i].style.backgroundPositionX = event.clientX / 25 + 'px'
        el2[i].style.backgroundPositionY = event.clientY / 25 + 'px'
      } else {
        el2[i].removeAttribute('style');
      }
    });
  }

}
