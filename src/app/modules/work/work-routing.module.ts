import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WorkComponent} from './work.component';
import {TimelineComponent} from '../timeline/timeline.component';
import {RoutersGuard} from '../../core/guard/routers.guard';


const routes: Routes = [
  {
    path: '',
    component: WorkComponent,
    canActivate: [RoutersGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkRoutingModule { }
