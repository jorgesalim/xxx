import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WorkRoutingModule } from './work-routing.module';
import {WorkComponent} from './work.component';
import {MenuModule} from '../../core/components/menu/menu.module';
import {AgmCoreModule} from '@agm/core';
import { FormsModule } from '@angular/forms';
import { MessageModule } from '../../core/components/message/message.module';

@NgModule({
  declarations: [
    WorkComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC5TvqWT-CqOV12BR8ccOWr8x_ooU8WdUc'
    }),
    CommonModule,
    WorkRoutingModule,
    FormsModule,
    MessageModule,
    MenuModule
  ]
})
export class WorkModule { }
