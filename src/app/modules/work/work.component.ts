import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ObraService} from '../../core/services/obra.service';
import {Obra} from '../../core/model/obra';
import {TrechoService} from '../../core/services/trecho.service';
import {Trecho} from '../../core/model/trecho';
import {DiarioService} from '../../core/services/diario.service';
import { ClassActive } from 'src/app/core/utils/classActive';
import { AuthService } from '../../core/services/auth.service';
import { User } from '../../core/model/user';
import { Diario } from '../../core/model/diario';
import { StatusDiario } from '../../core/model/statusdiario';
import { LaudosDiarios } from '../../core/model/laudosdiarios';
import { LaudosDiariosService } from '../../core/services/laudos-diarios.service';
import { MessageService } from '../../core/services/message.service';
import { Mensagem } from '../../core/model/mensagem';
import { UserService } from '../../core/services/user.service';
import { StatusTrecho } from '../../core/model/statustrecho';

@Component({
  selector: 'app-work',
  templateUrl: './work.component.html',
  styleUrls: ['./work.component.css']
})
export class WorkComponent implements OnInit {
  id: number;
  obra: Obra;
  maps: Obra[] = [];
  user: User;
  classActive: ClassActive;
  imgLightbox: string;
  statusDiario: StatusDiario;
  statusTrecho: StatusTrecho;
  counter = Array;
  inicio: Date;
  fim: Date;
  obraDetail = false;
  isPermition = false;
  obraRelatorio = false;
  isDiarioNaoAprovados = true;
  start = 0;
  limit = 5;
  activeDaily = '';
  activeTrecho = '';
  activeMaps = '';
  lat = -22.9116;
  lng = -43.1883;
  relatorio = {
    trechos: []
  };
  constructor(private route: ActivatedRoute,
              private authService: AuthService,
              public obraService: ObraService,
              public trechoService: TrechoService,
              public diarioService: DiarioService,
              public laudosDiariosService: LaudosDiariosService,
              private messageService: MessageService,
              public userService: UserService
            ) { }

  ngOnInit() {
    this.classActive = new ClassActive();
    this.statusDiario = new StatusDiario();
    this.statusTrecho = new StatusTrecho();
    this.user = new User();
    this.route.params.subscribe(params => {
      this.id = params.id;
      this.obraService.getById(this.id).subscribe(obra => {
        this.obra = obra;
        this.maps.push(obra);
      });
      this.diarioService.listSortByTrechoAndLaudoNaoAprovado(this.id)
        .subscribe(diarios => {
        if (diarios.length === 0) {
          this.isDiarioNaoAprovados = false;
        }
      });
    });
    this.setUser();
  }

  async gerarRelatorio() {
    this.getRelatorio();
    // this.openDetail();
    // console.log(' Gerando Relatório ');
  }
  filtrarPeriodo() {
    this.obra.trechos.forEach((t, index, array) => {
      this.diarioService.listSortByTrechoAndPeriodo(this.obra.id, t.id, this.inicio, this.fim)
        .subscribe((diarios) => {
          t.diarios = diarios;
          console.log(`filtrado pelo trecho ${t.nome}`);
        });
    });
  }

  async getDiarios(trecho) {
    if (!trecho.active || trecho.diarios) {
      return;
    }

    // this.diarioService.listSortByTrechoId(trecho.id, this.start, this.limit)
    this.diarioService.listAllSortByTrechoId(trecho.id)
      .subscribe(d => {
        trecho.diarios = d;

        // Temporário para resolver problema de paginação
        // trecho['lengthFinal'] = trecho.diarios.length ? trecho.diarios.length / 5 : 1;
        // trecho['lengthFinal'] = trecho['lengthFinal'] > 1 ? parseInt(trecho['lengthFinal']) : 1;
        // trecho.diarios = trecho.diarios.reverse().slice(0, 5);

        // Temporário pois precisa ajustar strapi para o coletor ser buscado pelo diario
        this.userService.getById(trecho.coletor)
          .subscribe(coletor => {
            trecho.coletor = coletor;
          });

        trecho.diarios.map((diario, indexD, arrayD) => {
          if (diario.laudosdiario) {
            this.laudosDiariosService.getById(+diario.laudosdiario.id)
              .subscribe(laudo => {
                if (laudo.parecer && laudo.parecer.id) {
                  this.userService.getById(laudo.parecer.id)
                    .subscribe(user => {
                      trecho.diarios[indexD].laudosdiario.parecer = user;
                    });
                }
              });
          }
        });
      });
  }

  async getRelatorio() {
    if (!this.obra) {
      return;
    }
    this.obraRelatorio = true;
    if (this.relatorio['obra']) {
      return;
    }

    let coletorId = 0;
    this.relatorio['obra'] = {};
    this.relatorio['trechos'] = [];
    this.relatorio['obra'] = Object.assign({}, this.obra);

    this.relatorio['obra'].trechos.map((trecho, indexD, arrayD) => {
      this.relatorio['trechos'][indexD] = Object.assign({}, trecho);
      this.diarioService.listAllSortByTrechoId(trecho.id).subscribe(d => {
        if (d) {
          this.relatorio['trechos'][indexD]['diarios'] = Object.assign([], d);
          coletorId = trecho['coletor'].id ? trecho['coletor'].id : trecho['coletor'];

          this.userService.getById(coletorId).subscribe(coletor => {
            this.relatorio['trechos'][indexD].coletor = Object.assign({}, coletor);

            this.relatorio['trechos'][indexD].diarios.map((diario, indexE, arrayE) => {
              if (diario.laudosdiario) {
                this.laudosDiariosService.getById(+diario.laudosdiario.id).subscribe(laudo => {
                  if (laudo.parecer && laudo.parecer.id) {
                    this.userService.getById(laudo.parecer.id).subscribe(user => {
                      this.relatorio['trechos'][indexD].diarios[indexE].laudosdiario.parecer = Object.assign({}, user);
                    });
                  }
                });
              }
            });
          });
        }
      });
    });
  }

  openLightbox(img) {
    this.imgLightbox = img;
  }

  closeLightbox() {
    this.imgLightbox = '';
  }

  openDetail() {
    this.obraDetail = true;
  }

  closeDetail() {
    this.obraDetail = false;
  }

  closeRelatorio() {
    this.obraRelatorio = false;
  }

  listMore(trecho: Trecho, index: number) {
    let start = index * 5;

    if (!trecho['dateFilterLTE'] || !trecho['dateFilterGTE']) {
      this.diarioService.listSortByTrechoId(trecho.id, start, this.limit)
        .subscribe(d => trecho.diarios = d);
      return;
    }
    this.diarioService.listSortByDateAndTrechoId(trecho.id, trecho['dateFilterGTE'], trecho['dateFilterLTE'], start, this.limit)
    .subscribe(d => trecho.diarios = d);
  }

  // Somente os tipos de usuários descritos no if estarão habilitados para aprovar ou reprovar os diário
  setUser() {
    let name = this.authService.authentication.user.role.name;
    this.user = this.authService.authentication.user;
    if (name === 'Fiscais' || name === 'Administrator') {
      this.isPermition = true;
    }
  }

  // Filtra diários pela data de criação
  filter(trecho: Trecho) {
    if (!trecho['dateFilterLTE'] || !trecho['dateFilterGTE']) {
      this.clearFilter(trecho);
      return;
    }
    this.diarioService.listSortByDateAndTrechoId(trecho.id, trecho['dateFilterGTE'], trecho['dateFilterLTE'], this.start, this.limit)
    .subscribe(d => trecho.diarios = d);
  }

  // Lipa filtro de data de diários
  clearFilter(trecho: Trecho) {
    trecho['dateFilterGTE'] = null;
    trecho['dateFilterLTE'] = null;
    this.diarioService.listSortByTrechoId(trecho.id, this.start, this.limit)
    .subscribe(d => trecho.diarios = d);
  }

  // Abre a section para descrever o motivo da reprovação
  openReasonSection(diario: any) {
    diario.laudosdiario = new LaudosDiarios();
    diario.laudosdiario.created_at = new Date();
    diario.laudosdiario.edit = 'edit';
  }

  // Primeiramente é executado um update do status do diário e em seguida é executado o método de insert do laudo
  updateDiario(aprovado: boolean, diario: Diario) {
    let status = aprovado ? this.statusDiario.aprovado : this.statusDiario.reprovado;
    let laudosdiario = {};

    if (!aprovado) {
      laudosdiario['descricao'] = diario.laudosdiario.descricao;
    }

    laudosdiario['status'] = status;
    laudosdiario['diario'] = diario.id;
    laudosdiario['parecer'] = this.authService.authentication.user.id;

    this.diarioService.approveOrDisapprove(diario.id, status)
    .subscribe(d => this.saveLaudo(laudosdiario, status, diario));
  }

  // Executa o insert do laudo
  saveLaudo(laudosdiario: any, status: string, diario: Diario) {
    this.laudosDiariosService.insertLaudo(laudosdiario)
      .subscribe(l => {
        this.setMessage('done', 'Diário atualizado com sucesso', 'active');
        diario.status = status;
        diario.laudosdiario = diario.laudosdiario ? diario.laudosdiario : new LaudosDiarios();
        diario.laudosdiario.status = status;
        diario.laudosdiario.parecer = this.user;
      });
  }

  // Status do trecho
  status(value) {
    if (value === this.statusTrecho.aguardando) {
      return 'not-started';
    } else if (value === this.statusTrecho.construindo) {
      return 'in-progress';
    } else {
      return 'finalized';
    }
  }

  // Status do diário
  diarioStatus(value) {
    if (value === this.statusDiario.aprovado) {
      return 'approved';
    } else if (value === this.statusDiario.reprovado) {
      return 'disapproved';
    }
  }

  active(type) {
    if (type === 'trechoInsert') {
      this.activeTrecho = this.activeTrecho === 'active' ? '' : 'active';
    } else if (type === 'maps') {
      this.activeMaps = this.activeMaps === 'active' ? '' : 'active';
    } else {
      this.activeDaily = this.activeDaily === 'active' ? '' : 'active';
    }
  }

  collapseMaps(collapse) {
    const elementMap = this.newElementType(this.getMapTag());
    if (collapse) {
      elementMap.style.display = 'block';
    } else {
      elementMap.style.display = 'none';
    }
  }

  getMapTag() {
    return document.getElementsByTagName('agm-map')[0];
  }

  newElementType(el) {
    return el;
  }

  private setMessage(type: string, description: string, active: string) {
    this.messageService.add(new Mensagem(type, description, active));
  }

}
