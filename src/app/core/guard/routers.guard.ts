import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthService} from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class RoutersGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (localStorage.getItem('auth')) {
      this.authService.authentication = JSON.parse(localStorage.getItem('auth'));
      return true;
    }
    localStorage.removeItem('auth');
    this.router.navigate(['/'], { queryParams: { returnUrl: state.url }});
    return false;
  }
}
