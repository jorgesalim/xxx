import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterContentChecked {

  isLogin = false;
  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  ngAfterContentChecked() {
    if (this.authService.authentication) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
  }

}
