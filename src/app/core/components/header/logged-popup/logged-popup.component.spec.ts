import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoggedPopupComponent } from './logged-popup.component';

describe('LoggedPopupComponent', () => {
  let component: LoggedPopupComponent;
  let fixture: ComponentFixture<LoggedPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoggedPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
