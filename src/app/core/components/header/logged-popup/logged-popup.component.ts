import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-logged-popup',
  templateUrl: './logged-popup.component.html',
  styleUrls: ['./logged-popup.component.css']
})
export class LoggedPopupComponent implements OnInit {

  uName: string;
  uEmail: string;
  isActive = false;

  constructor(
    private el: ElementRef,
    private authService: AuthService
  ) { }

  @HostListener('document:click', ['$event'])
  onClickOutside($event) {
    const clickedInside = this.el.nativeElement.contains($event.target);
    if (!clickedInside) {
      this.isActive = false;
    }
  }

  ngOnInit() {
    this.getUserSet();
  }

  getUserSet() {
    const uName = this.authService.authentication.user.nome;
    const uEmail = this.authService.authentication.user.email;

    this.uName = uName;
    this.uEmail = uEmail;
  }

  openPanel() {
    this.isActive = !this.isActive;
  }

  logOut() {
    localStorage.clear();
    location.reload();
  }

}
