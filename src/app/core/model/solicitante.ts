import {Executora} from './executora';
import {Image} from './image';
import {Fiscal} from './fiscal';


export class Solicitante {
  id: number;
  nome: string;
  cep: number;
  endereco: string;
  bairro: string;
  numero: number;
  complemento: string;
  cidade: string;
  estado: string;
  executora: Executora;
  logo: Image;
  fiscais: Fiscal[];
  createdAt: string;
  updatedAt: string;
}
