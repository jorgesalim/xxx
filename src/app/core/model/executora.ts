import {Solicitante} from './solicitante';
import {Obra} from './obra';
import {User} from './user';


export class Executora {
  id: number;
  nome: string;
  logradouro: string;
  bairro: string;
  cidade: string;
  cep: number;
  estado: string;
  createdAt: string;
  updatedAt: string;
  complemento: string;
  numero: number;
  cnpj: string;
  obras: Obra[];
  solicitantes: Solicitante[];
  engenheiros: User[];
  geologos: User[];
  coletores: User[];
  active: string;
  edit: string;
  disable = true;
}
