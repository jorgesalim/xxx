import {Solicitante} from './solicitante';
import {User} from './user';

export class Fiscal {
    id: number;
    createdAt: string;
    updatedAt: string;
    nomecompleto: string;
    cpf: string;
    tipo: string;
    crea: string;
    user: User;
    sobrenome: string;
    nome: string;
    solicitante: Solicitante;
}
