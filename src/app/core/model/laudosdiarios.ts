import {Diario} from './diario';
import {User} from './user';

export class LaudosDiarios {
  id: number;
  createdAt: string;
  updatedAt: string;
  status: string;
  descricao: string;
  diario: Diario;
  parecer: User;
  active: string;
  edit: string;
  disable = true;
}
