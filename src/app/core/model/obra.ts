import {Executora} from './executora';
import {Trecho} from './trecho';
import {Image} from './image';
import {User} from './user';
import {Solicitante} from './solicitante';
import {Fiscal} from './fiscal';


export class Obra {
  id: number;
  nomeobra: string;
  endereco: string;
  numero: number;
  cep: string;
  complemento: string;
  cidade: string;
  bairro: string;
  estado: string;
  fiscalizacaosolicitante: string;
  datainicio: string;
  datafim: string;
  contrato: string;
  qtdempregosdir: number;
  qtdempregosindi: number;
  nomeengencivil: string;
  creaengencivil: string;
  nomegeologo: string;
  creageologo: string;
  valorobra: number;
  qtdtrechos: number;
  status: string;
  executora: Executora;
  createdAt: string;
  updatedAt: string;
  imagemsolicitante: Image;
  trechos: Trecho[];
  latitude: string;
  longitude: string;
  engenheiro: User;
  geologo: User;
  active: string;
  edit: string;
  disable = true;
  solicitante: Solicitante;
  fiscal: Fiscal;
  user: User;
}
