export class StatusDiario {

    constructor(
        public readonly aprovado = 'Aprovado',
        public readonly reprovado = 'Reprovado',
        public readonly naoIniciado = 'Não iniciado',
        public readonly emAndamento = 'Em andamento'
    ) { }
}
