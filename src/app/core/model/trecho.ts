import {Obra} from './obra';
import {Diario} from './diario';

export class Trecho {
  id: number;
  nome: string;
  obra: Obra;
  descricao: string;
  status: string;
  diarios: Diario[];
  createdAt: string;
  updatedAt: string;
  active: string;
  edit: string;
  disable = true;
}
