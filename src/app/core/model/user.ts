import {Role} from './role';

import {Solicitante} from './solicitante';
import { Executora } from './executora';


export class User {
  username: string;
  id: number;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  role: Role;
  nome: string;
  sobrenome: string;
  cpf: string;
  sexo: string;
  executoras: Executora;
  nascimento: string;
  estadoCivil: string;
  solicitante: Solicitante;
  crea: string;
  telefone: string;
}
