export class StatusTrecho {

    constructor(
        public readonly aguardando = 'Aguardando',
        public readonly construindo = 'Construindo'
    ) { }
}
