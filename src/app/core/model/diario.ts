import {Image} from './image';
import {User} from './user';
import {Trecho} from './trecho';
import { LaudosDiarios } from './laudosdiarios';

export class Diario {
    id: number;
    nome: string;
    descricao: string;
    data: string;
    status: string;
    registroimagem: Image[];
    user: User;
    trecho: Trecho;
    createdAt: string;
    updatedAt: string;
    laudosdiario: LaudosDiarios
}
