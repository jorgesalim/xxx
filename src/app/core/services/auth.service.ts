import { Injectable } from '@angular/core';
import {Auth} from '../model/auth';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Login} from '../model/login';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {User} from '../model/user';
import {throwError} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private API = 'http://18.234.23.182:1337/auth/local';

  authentication: Auth;
  constructor(private http: HttpClient) { }

  login(login: Login) {

    const options = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<Auth>( this.API, login, options)
      .pipe(
        map(response => {
          if (response.user.confirmed) {
            this.authentication = response;
          }
          return response;
        }),
        delay(2000),
        tap(console.log),
        catchError(this.handleError)
      );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
