import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Diario} from '../model/diario';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {LaudosDiarios} from '../model/laudosdiarios';

@Injectable({
  providedIn: 'root'
})
export class DiarioService {

  private API = 'http://18.234.23.182:1337/diarios';
  private httpOptions = {};

  constructor(private http: HttpClient) {
  }

  list() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario[]>(this.API, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByDate(date, start, limit): Observable<Diario[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario[]>(`${this.API}?updatedAt_lt=${date}&_sort=updatedAt:DESC&_start=${start}&_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByDateAndTrechoId(trechoId, dateGTE, dateLTE, start, limit): Observable<Diario[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario[]>(`${this.API}
    ?trecho.id=${trechoId}
    &created_at_gte=${dateGTE}
    &created_at_lte=${dateLTE}
    &_sort=created_at:DESC
    &_start=${start}
    &_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByTrechoAndLaudoNaoAprovado(obraId): Observable<Diario[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario[]>(`${this.API}?trecho.obra=${obraId}&_sort=trecho:ASC&laudosdiario.status_ne=Aprovado`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByTrechoAndPeriodo(obraId, trechoId, dateGTE, dateLTE): Observable<Diario[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario[]>(`${this.API}?trecho.id=${trechoId}&trecho.obra=${obraId}&datadiario_gte=${dateGTE}&datadiario_lte=${dateLTE}&_sort=trecho:ASC`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByTrechoId(trechoId, start, limit): Observable<Diario[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario[]>(`${this.API}?trecho.id=${trechoId}&_sort=created_at:DESC&_start=${start}&_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listAllSortByTrechoId(trechoId): Observable<Diario[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario[]>(`${this.API}?trecho.id=${trechoId}&_sort=created_at:DESC`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  getById(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Diario>(`${this.API}/${id}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  update(diario: Diario): Observable<Diario> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<Diario>(`${this.API}/${diario.id}`, diario, this.httpOptions).pipe(
      tap(_ => console.log(`Update Diario:  ${diario.id}`)),
      catchError(this.handleError)
    );
  }

  approveOrDisapprove(diarioId: number, status: string): Observable<Diario> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<Diario>(`${this.API}/${diarioId}`, {status}, this.httpOptions).pipe(
      tap(_ => console.log(`Update Diario:  ${diarioId}`)),
      catchError(this.handleError)
    );
  }

  insert(diario: Diario): Observable<Diario> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<Diario>(`${this.API}`, diario, this.httpOptions).pipe(
      tap(_ => console.log(`Insert new Diario:  ${diario.id}`)),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
