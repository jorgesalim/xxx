import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Executora} from '../model/executora';
import {Obra} from '../model/obra';

@Injectable({
  providedIn: 'root'
})
export class ExecutoraService {

  private API = 'http://18.234.23.182:1337/executoras';
  private httpOptions = {};

  constructor(private http: HttpClient) { }

  list() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Executora[]>(`${this.API}?_sort=updated_at:DESC`, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listLimit(start, limit): Observable<Executora[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Executora[]>(`${this.API}?_sort=updated_at:DESC&_start=${start}&_limit=${limit}`,
        this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listSortByDate(date, start, limit): Observable<Executora[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Executora[]>(`${this.API}?updatedAt_lt=${date}&_sort=updated_at:DESC&_start=${start}&_limit=${limit}`,
        this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listSortUpDatedDesc(start, limit): Observable<Executora[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Executora[]>(`${this.API}?_sort=updated_at:DESC&_start=${start}&_limit=${limit}`,
        this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listByNameLike(name: string) {
      this.httpOptions = {
          headers: new HttpHeaders({
              'Content-Type':  'application/json',
              Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
          })
      };
      return this.http.get<Executora[]>(`${this.API}?nome_contains=${name}`, this.httpOptions).pipe(
          catchError(this.handleError)
      );
  }

  listByBairroLike(name: string) {
      this.httpOptions = {
          headers: new HttpHeaders({
              'Content-Type':  'application/json',
              Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
          })
      };
      return this.http.get<Executora[]>(`${this.API}?bairro_contains=${name}`, this.httpOptions).pipe(
          catchError(this.handleError)
      );
  }

  getById(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Executora>(`${this.API}/${id}`, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  update(executora: Executora): Observable<Executora> {
    this.deleteAttributes(executora);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<Executora>( `${this.API}/${executora.id}`, executora, this.httpOptions ).pipe(
        tap(_ => console.log(`Update Executora:  ${executora.id}`)),
        catchError(this.handleError)
    );
  }

  insert(executora: Executora): Observable<Executora> {
    this.deleteAttributes(executora);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<Executora>( `${this.API}`, executora, this.httpOptions ).pipe(
        tap(_ => console.log(`Insert new Executora:  ${executora.id}`)),
        catchError(this.handleError)
    );
  }

  delete(executora: Executora) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.delete( `${this.API}/${executora.id}`, this.httpOptions).pipe(
        tap(_ => console.log(`Insert new Executora:  ${executora.id}`)),
        catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  private deleteAttributes(executora: Executora) {
    delete executora.active;
    delete executora.edit;
    delete executora.disable;
  }
}
