import { TestBed } from '@angular/core/testing';

import { ExecutoraService } from './executora.service';

describe('ExecutoraService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ExecutoraService = TestBed.get(ExecutoraService);
    expect(service).toBeTruthy();
  });
});
