import { TestBed } from '@angular/core/testing';

import { TrechoService } from './trecho.service';

describe('TrechoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TrechoService = TestBed.get(TrechoService);
    expect(service).toBeTruthy();
  });
});
