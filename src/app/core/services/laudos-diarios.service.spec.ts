import { TestBed } from '@angular/core/testing';

import { LaudosDiariosService } from './laudos-diarios.service';

describe('LaudosDiariosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LaudosDiariosService = TestBed.get(LaudosDiariosService);
    expect(service).toBeTruthy();
  });
});
