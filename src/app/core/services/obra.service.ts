import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Obra} from '../model/obra';
import {Executora} from '../model/executora';

@Injectable({
  providedIn: 'root'
})
export class ObraService {

  private API = 'http://18.234.23.182:1337/obras';
  private httpOptions = {};

  constructor(private http: HttpClient) {
  }

  list() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(this.API, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listLimit(start, limit): Observable<Obra[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(`${this.API}?_sort=updated_at:DESC&_start=${start}&_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByDate(date, start, limit): Observable<Obra[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(`${this.API}?_start=${start}&_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortUpDatedDesc(start, limit): Observable<Obra[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(`${this.API}?_sort=updated_at:DESC&_start=${start}&_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listByNameLike(name: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(`${this.API}?nomeobra_contains=${name}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listByNomeObraLike(name: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(`${this.API}?nomeobra_contains=${name}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listByBairroLike(name: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(`${this.API}?bairro_contains=${name}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listLimitByExecutoraIdLike(id, start, limit) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra[]>(`${this.API}?executora.id=${id}&_start=${start}&_limit=${limit}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  getById(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Obra>(`${this.API}/${id}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  update(obra: Obra): Observable<Obra> {
    this.deleteAttributes(obra);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<Obra>(`${this.API}/${obra.id}`, obra, this.httpOptions).pipe(
      tap(_ => console.log(`Update Obra:  ${obra.id}`)),
      catchError(this.handleError)
    );
  }

  insert(obra: Obra): Observable<Obra> {
    this.deleteAttributes(obra);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<Obra>(`${this.API}`, obra, this.httpOptions).pipe(
      tap(_ => console.log(`Insert new Obra:  ${obra.id}`)),
      catchError(this.handleError)
    );
  }

  delete(obra: Obra) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.delete( `${this.API}/${obra.id}`, this.httpOptions).pipe(
      tap(_ => console.log(`Insert new Obra:  ${obra.id}`)),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  private deleteAttributes(obra: Obra) {
    delete obra.active;
    delete obra.edit;
    delete obra.disable;
    delete obra.executora.active;
    delete obra.executora.edit;
    delete obra.executora.disable;
  }
}
