import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {tap} from 'rxjs/operators';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {LaudosDiarios} from '../model/laudosdiarios';
import {catchError} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class LaudosDiariosService {

  private API = 'http://18.234.23.182:1337/laudosdiarios';
  private httpOptions = {};

  constructor(private http: HttpClient) {
  }

  list() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(this.API, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listLimit(start, limit): Observable<LaudosDiarios[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?_start=${start}&_limit=${limit}&_sort=updated_at:ASC`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByDate(date, start, limit): Observable<LaudosDiarios[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?_start=${start}&_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortUpDatedDesc(start, limit): Observable<LaudosDiarios[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?_sort=updated_at:DESC&_start=${start}&_limit=${limit}`,
      this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listSortByDiarioId(diarioId, start, limit): Observable<LaudosDiarios[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?diario.id=${diarioId}&_sort=created_at:DESC&_start=${start}&_limit=${limit}`,
        this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listByNameLike(name: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?nomeLaudosDiarios_contains=${name}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listByNomeLaudosDiariosLike(name: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?nomeLaudosDiarios_contains=${name}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listByBairroLike(name: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?bairro_contains=${name}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  listLimitByExecutoraIdLike(id: number, start, limit) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios[]>(`${this.API}?executora.id=${id}&_start=${start}&_limit=${limit}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  getById(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<LaudosDiarios>(`${this.API}/${id}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }

  update(laudosDiarios: LaudosDiarios): Observable<LaudosDiarios> {
    this.deleteAttributes(laudosDiarios);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<LaudosDiarios>(`${this.API}/${laudosDiarios.id}`, laudosDiarios, this.httpOptions).pipe(
      tap(_ => console.log(`Update LaudosDiarios:  ${laudosDiarios.id}`)),
      catchError(this.handleError)
    );
  }

  insert(laudosDiarios: LaudosDiarios): Observable<LaudosDiarios> {
    this.deleteAttributes(laudosDiarios);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<LaudosDiarios>(`${this.API}`, laudosDiarios, this.httpOptions).pipe(
      tap(_ => console.log(`Insert new LaudosDiarios:  ${laudosDiarios.id}`)),
      catchError(this.handleError)
    );
  }

  insertLaudo(laudosDiarios: any): Observable<LaudosDiarios> {
    this.deleteAttributes(laudosDiarios);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<LaudosDiarios>(`${this.API}`, laudosDiarios, this.httpOptions).pipe(
      tap(_ => console.log(`Insert new LaudosDiarios:  ${laudosDiarios.id}`)),
      catchError(this.handleError)
    );
  }

  delete(laudosDiarios: LaudosDiarios) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.delete( `${this.API}/${laudosDiarios.id}`, this.httpOptions).pipe(
      tap(_ => console.log(`Insert new LaudosDiarios:  ${laudosDiarios.id}`)),
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  private deleteAttributes(laudosDiarios: LaudosDiarios) {
    delete laudosDiarios.active;
    delete laudosDiarios.edit;
    delete laudosDiarios.disable;
  }
}
