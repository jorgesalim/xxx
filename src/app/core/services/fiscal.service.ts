import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Fiscal} from '../model/fiscal';

@Injectable({
  providedIn: 'root'
})
export class FiscalService {

  private API = 'http://18.234.23.182:1337/fiscals';
  private httpOptions = {};

  constructor(private http: HttpClient) { }

  list() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Fiscal[]>(this.API, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listSortByDate(date, start, limit): Observable<Fiscal[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Fiscal[]>(`${this.API}?updatedAt_lt=${date}&_sort=updatedAt:DESC&_start=${start}&_limit=${limit}`,
        this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  getById(id: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Fiscal>(`${this.API}/${id}`, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  update(fiscal: Fiscal): Observable<Fiscal> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<Fiscal>( `${this.API}/${fiscal.id}`, fiscal, this.httpOptions ).pipe(
        tap(_ => console.log(`Update Fiscal:  ${fiscal.id}`)),
        catchError(this.handleError)
    );
  }

  insert(fiscal: Fiscal): Observable<Fiscal> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<Fiscal>( `${this.API}`, fiscal, this.httpOptions ).pipe(
        tap(_ => console.log(`Insert new Fiscal:  ${fiscal.id}`)),
        catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
