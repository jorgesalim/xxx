import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Trecho} from '../model/trecho';
import {Obra} from '../model/obra';

@Injectable({
  providedIn: 'root'
})
export class TrechoService {

  private API = 'http://18.234.23.182:1337/trechos';
  private httpOptions = {};

  constructor(private http: HttpClient) { }

  list() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Trecho[]>(`${this.API}?_sort=updated_at:DESC`, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listSortByDate(date, start, limit): Observable<Trecho[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Trecho[]>(`${this.API}?updatedAt_lt=${date}&_sort=updatedAt:DESC&_start=${start}&_limit=${limit}`,
        this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  getById(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Trecho>(`${this.API}/${id}`, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  update(trecho: Trecho): Observable<Trecho> {
    this.deleteAttributes(trecho);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<Trecho>( `${this.API}/${trecho.id}`, trecho, this.httpOptions ).pipe(
        tap(_ => console.log(`Update Trecho:  ${trecho.id}`)),
        catchError(this.handleError)
    );
  }

  insert(trecho: Trecho): Observable<Trecho> {
    this.deleteAttributes(trecho);
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<Trecho>( `${this.API}`, trecho, this.httpOptions ).pipe(
        tap(_ => console.log(`Insert new Trecho:  ${trecho.id}`)),
        catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  private deleteAttributes(trecho: Trecho) {
    delete trecho.active;
    delete trecho.edit;
    delete trecho.disable;
  }
}
