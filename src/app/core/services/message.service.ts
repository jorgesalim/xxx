import {Injectable, Input} from '@angular/core';
import {Mensagem} from '../model/mensagem';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public mensagem: Mensagem;

  add(mensagem: Mensagem) {
    this.mensagem = mensagem;
    setTimeout(() =>{ this.clear() }, 3000);
  }

  clear() {
    this.mensagem = null;
  }
}
