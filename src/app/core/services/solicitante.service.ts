import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {catchError, delay, map, tap} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import {Solicitante} from '../model/solicitante';

@Injectable({
  providedIn: 'root'
})
export class SolicitanteService {

  private API = 'http://18.234.23.182:1337/solicitantes';
  private httpOptions = {};

  constructor(private http: HttpClient) { }

  list() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Solicitante[]>(this.API, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  listSortByDate(date, start, limit): Observable<Solicitante[]> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Solicitante[]>(`${this.API}?updatedAt_lt=${date}&_sort=updatedAt:DESC&_start=${start}&_limit=${limit}`,
        this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  getById(id: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<Solicitante>(`${this.API}/${id}`, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }

  update(solicitante: Solicitante): Observable<Solicitante> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.put<Solicitante>( `${this.API}/${solicitante.id}`, solicitante, this.httpOptions ).pipe(
        tap(_ => console.log(`Update Solicitante:  ${solicitante.id}`)),
        catchError(this.handleError)
    );
  }

  insert(solicitante: Solicitante): Observable<Solicitante> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.post<Solicitante>( `${this.API}`, solicitante, this.httpOptions ).pipe(
        tap(_ => console.log(`Insert new Solicitante:  ${solicitante.id}`)),
        catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }
}
