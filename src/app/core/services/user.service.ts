import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';

import {catchError, delay, map, tap} from 'rxjs/operators';
import {throwError} from 'rxjs';
import {Auth} from '../model/auth';
import {Login} from '../model/login';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private API = 'http://18.234.23.182:1337/users';
  private httpOptions = {};

  constructor(private http: HttpClient) { }

  listByRoleName(roleName: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<User[]>(`${this.API}?role.name=${roleName}`, this.httpOptions).pipe(
      catchError(this.handleError)
    );
  }
  private handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  getById(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('auth')).jwt}`
      })
    };
    return this.http.get<User>(`${this.API}/${id}`, this.httpOptions).pipe(
        catchError(this.handleError)
    );
  }
}
