export class Status {
  change(value) {
    if (value === 'Aguardando') {
      return 'not-started';
    } else if (value === 'Construindo') {
        return 'in-progress';
    } else {
        return 'finalized';
    }
  }
}
