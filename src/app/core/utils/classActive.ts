export class ClassActive {
  active(type) {
    return type === 'active' ? '' : 'active';
  }

  edit(type) {
    return type === 'editing' ? '' : 'editing';
  }

  disable(type) {
    return type ? false : true;
  }
}
