import { Component } from '@angular/core';
import {NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {Auth} from './core/model/auth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'geoflow-daily-backoffice';

  loading = false;

  mainClass = 'login';

  auth: Auth;

  constructor(private router: Router) {
    this.auth = JSON.parse(localStorage.getItem('auth'));
    this.router.events.subscribe((event) => {
      function getUrl(object) {
        if (object.url) {
          return object.url;
        } else {
          return '';
        }
      }

      switch (true) {
        case event instanceof NavigationStart: {
          this.loading = true;
          if (getUrl(event).includes('returnUrl')) {
            this.mainClass = 'login';
          } else if (getUrl(event).includes('work')) {
              this.mainClass = 'work';
          } else if (getUrl(event).includes('time-line')) {
            this.mainClass = 'time-line';
          } else if (getUrl(event).includes('dashboard')) {
            this.mainClass = 'dashboard';
          } else {
            this.mainClass = 'login';
          }
          if (this.auth && this.auth.user && this.auth.user.role.name === 'Executora') {
            if (getUrl(event) !== ('/dashboard/executora/obras')) {
              this.router.navigate(['/dashboard/executora/obras']);
            }
          }

          break;
        }

        case event instanceof NavigationEnd:
        case event instanceof NavigationCancel:
        case event instanceof NavigationError: {
          this.loading = false;
          break;
        }
        default: {
          break;
        }
      }
    });
  }


}
