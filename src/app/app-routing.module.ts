import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './modules/login/login.module#LoginModule'
  },
  {
    path: 'o-produto',
    loadChildren: './modules/page/produto/produto.module#ProdutoModule'
  },
  {
    path: 'ajuda',
    loadChildren: './modules/page/ajuda/ajuda.module#AjudaModule'
  },
  {
    path: 'time-line',
    loadChildren: './modules/timeline/timeline.module#TimelineModule'
  },
  {
    path: 'time-line/:id/work',
    loadChildren: './modules/work/work.module#WorkModule'
  },
  {
    path: 'dashboard/solicitante/executoras',
    loadChildren: './modules/dashboard/solicitante/executoras/executoras.module#ExecutorasModule'
  },
  {
    path: 'dashboard/solicitante/obras',
    loadChildren: './modules/dashboard/solicitante/obras/obras.module#ObrasModule'
  },
  {
    path: 'dashboard/solicitante/trechos',
    loadChildren: './modules/dashboard/solicitante/trechos/trechos.module#TrechosModule'
  },
  {
    path: 'dashboard/solicitante/usuarios',
    loadChildren: './modules/dashboard/solicitante/usuarios/usuarios.module#UsuariosModule'
  },
  {
    path: 'dashboard/executora/obras',
    loadChildren: './modules/dashboard/executora/obras-executora/obras-executora.module#ObrasExecutoraModule'
  },
  {
    path: 'dashboard/executora/usuarios',
    loadChildren: './modules/dashboard/executora/usuarios-executora/usuarios-executora.module#UsuariosExecutoraModule'
  },
  {
    path: '**',
    loadChildren: './modules/page-not-found/page-not-found.module#PageNotFoundModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{ 
    preloadingStrategy: PreloadAllModules 
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
